CC=gcc
CFLAGS= -I -Wall -l ncurses -l sqlite3 -g
DEPS = window.h yearmonthday.h
OBJ = calendar.o

%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

calendar: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)
