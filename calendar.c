#include <stdio.h>
#include <ncurses.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <sqlite3.h>
#include <time.h>
#include "yearmonthday.h"
#include "window.h"

sqlite3 *db;

static int callback(void *NotUsed, int argc, char **argv, char **azColName) {
	WINDOW *reply = drawWindow(40, 40, 10, 10);
			refresh();
			box(reply, 0, 0);
	//~ move(50, 11);
	for(int i = 0; i<argc; i++) {
		
		printw("%s = %s", azColName[i], argv[i] ? argv[i] : "NULL");
		wrefresh(reply);
	}
	return 0;
}

int doesFileExist(const char *filename){
	if( access( filename, F_OK ) != -1 ) {
		return 1;
	} else {
		return 0;
	}
}

int createDatabease(const char *filename){
	char *zErrMsg = 0;
	char *sql;
	
	/* Open database */
	sqlite3_open(filename, &db);
	
	/* Create SQL statement */
	sql = "CREATE TABLE Event("  \
	"Date char," \
	"Discription char," \
	"Timeframe char );";

	/* Execute SQL statement */
	sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	
	return 1;
}

//Insert new element into database (Event table)
void createEvent(){
	char date[10];
	char discription[300];
	char timeframe[20];
	char *zErrMsg = 0;
	char *sql;
	
	endwin();
	clear();
	
	printf("Date:");
	//~ getch();
	fgets(date, 10, stdin);
	printf("Discription:");
	fgets(discription, 10, stdin);
	printf("Timeframe:");
	fgets(timeframe, 10, stdin);
	
	/* Create SQL statement */
	sql = sqlite3_mprintf("INSERT INTO Event (Date, Discription, Timeframe) VALUES ('%q', '%q', '%q');", date, discription, timeframe);
		 
	/* Execute SQL statement */
	sqlite3_exec(db, sql, callback, 0, &zErrMsg);
   
   initscr();
	
}

void calendarVisualLoop(){
	clear();
	WINDOW *menu = drawWindow(9,23,5,10);
	keypad(menu, true);
	
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	int *days = dayListForMonth(tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
	
	int key;
	int highlight = 0;
	while (days[highlight] == 0)
	    highlight++;
	int day;
	while(key != 10){
	    int xp = 3;
	    int yp = 3;
	    for(int i = 0; i < 42; i++){
		if(i == highlight){
	    	wattron(menu, A_REVERSE);
		day = i;
		}
					
		mvwprintw(menu, 1, 1,  "   %s  ", months[tm.tm_mon + 1]);
		mvwprintw(menu, 1, 15,  "%i", tm.tm_year + 1900);
		mvwprintw(menu, 2, 1,  "ma di wo do vr za zo");
					
		if (days[i] != 0){
		    if (days[i]/10 != 0)
			mvwprintw(menu, yp, xp, "%i", days[i]);
			else
			    mvwprintw(menu, yp, xp, " %i", days[i]);

			if (i%7 == 0){
			    xp = 1, yp++;
			}
			else{
			    if (days[i]/10 != 0)
				xp += 3;
			    else
				xp += 3;
			}
		}
		else
		    xp += 2;			
		    
		wattroff(menu, A_REVERSE);
	    }
	    key = wgetch(menu);
			
	    switch(key){
		case KEY_UP :
		    if (days[highlight - 7] != 0)
			highlight -= 7;
		    break;
		case KEY_DOWN :
		    if (highlight + 7 <= 42 && days[highlight + 7] != 0)
			highlight += 7;
		    break;
		case KEY_LEFT :
		    if (days[highlight - 1] != 0)
			highlight--;
		    break;
		case KEY_RIGHT :
		    if (days[highlight + 1] != 0)
			highlight++;
		    break;
		    
	    }
	}
	
	if (day == 0){
		getch();
	}
	
	
	getch();
}

void programLoop(){
	int running = 1;
	char *zErrMsg = 0;
	char *sql;
	const char* data = "Callback function called";
	
	while (running){
		clear();
		WINDOW *menu = drawWindow(5, 10, 1, 1);
		keypad(menu, true);
		
		char *commands[3] = {"calendar","show","exit"};
		int key;
		int command;
		int highlight = 0;
		
		//selecting and highlighting a command from the menu
		while(key != 10){
				for(int i = 0; i < 3; i++){
					if(i == highlight){
						wattron(menu, A_REVERSE);
						command = i;
					}
					mvwprintw(menu, i+1, 1, commands[i]);
					wattroff(menu, A_REVERSE);
				}
			key = wgetch(menu);
			
			switch(key){
					case KEY_UP :
						highlight--;
						if (highlight == -1)
							highlight = 0;
						break;
					case KEY_DOWN :
						highlight++;
						if (highlight == sizeof(commands)/sizeof(commands[0]))
							highlight = sizeof(commands)/sizeof(commands[0]) - 1;
						break;
					default:
						break;
			}
		}
		key = 0;
		
		if(command == 0){
			calendarVisualLoop();
		}
		else if(command == 1){
			// show events (filtered)
			clear();
			endwin();
			/* Create SQL statement */
			sql = "SELECT * from Event";
		   
			/* Execute SQL statement */
			sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
					
			getch();
			//~ clear();
		}
		else if(command == 2){
			running = 0;
		}
		clear();
	}
	
}



int main(int argc, char* argv[]){
	//~ dayListForMonth(2019, 2, 1);
	//initiating ncurses
	initscr();
	cbreak();
	
	//~ char *zErrMsg = 0;
	//~ char *sql;
   
	char *filename = "event.db";
	
	if (doesFileExist(filename)){
		/* Open database */
		sqlite3_open(filename, &db);
	}
	else{
		createDatabease(filename);
		
	}
	
	programLoop();
	//~ //sqlite3_exec(db, sql, callback, 0, &zErrMsg);
	sqlite3_close(db);
	endwin();
	return 0;
}
