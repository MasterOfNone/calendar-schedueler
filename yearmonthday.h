#include <stdio.h>
#include <time.h>

int days_in_month[13]={0,31,28,31,30,31,30,31,31,30,31,30,31};

char *months[]=
{
	" ",
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
};

int isLeapYear(int year){
	if (year%100 == 0 && year%400 != 0)
		return 0;
	else if (year%4)
		return 0;
	else
		return 1;
}

int daysInMonth(int month, int year){
		if (month == 2 && isLeapYear(year))
			return days_in_month[month] + 1;
		return days_in_month[month];
}

int * dayList(){
		static int days[42] =  {0, 0, 0, 0, 0, 0, 0,\
								0, 0, 0, 0, 0, 0, 0,\
								0, 0, 0, 0, 0, 0, 0,\
								0, 0, 0, 0, 0, 0, 0,\
								0, 0, 0, 0, 0, 0, 0,\
								0, 0, 0, 0, 0, 0, 0};
		return days;
}

int dow(int y, int m, int d){
  static int t[] = {0, 3, 2, 5, 0, 3, 5, 1, 4, 6, 2, 4};
  y -= m < 3;
  return (y + y/4 - y/100 + y/400 + t[m-1] + d - 1) % 7;
}

int * dayListForMonth(int year, int month, int day){
	int *days = dayList();
	
	days[dow(year, month, day)] = 1;
	
	int i = 0;
	while (days[i] == 0){
		i++;
	}
	
	int n = 1;
	for (i = i; i < 42; i++){
		if (n <= daysInMonth(month, year))
			days[i] = n++;
	}
	
	return days;
}
