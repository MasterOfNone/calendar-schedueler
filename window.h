#include <stdio.h>
#include <ncurses.h>
#include <stdbool.h>
#include <sqlite3.h>

//Draw a new window in the terminal >>> refresh the terminal
WINDOW *drawWindow(int height, int width, int start_y, int start_x){
	WINDOW * win = newwin(height, width, start_y, start_x);
	refresh();
	
	box(win, 0, 0);
	wrefresh(win);
	refresh();
	return win;
}

//Write text in window >>> refresh window
void writeInWindow(WINDOW *win, char *text, int cursor_y, int cursor_x){
	mvwprintw(win, cursor_y, cursor_x, text);
	wrefresh(win);
	
}
